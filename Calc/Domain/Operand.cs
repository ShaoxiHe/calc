﻿using System;
using Calc.Enums;
using Calc.Exceptions;

namespace Calc.Domain
{
    /// <summary>
    /// Represent an Operand in the evaluation process.
    /// Adding and deduction between numbers and variables will be treated as
    /// one whole complex operand
    /// </summary>
    public class Operand
    {
        public int ConstantValue { get; set; }
        public int Coefficient { get; set; }
        public int QuadraticCoefficient { get; set; }

        public int DenominatorCoefficient { get; set; }
        public int DenominatorQuadraticCoefficient { get; set; }
        public int DenominatorConstantValue { get; set; }

        public Operand()
        {
            ConstantValue = 0;
            Coefficient = 0;
            QuadraticCoefficient = 0;

            DenominatorCoefficient = 0;
            DenominatorQuadraticCoefficient = 0;
            DenominatorConstantValue = 1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Calc.Domain.Operand"/> class.
        /// Not empty operand is allowed.
        /// It's either a number or a variable.
        /// </summary>
        /// <param name="token">Token.</param>
        public Operand(Token token)
        {
            switch (token.TypeEnum)
            {
                case TokenEnum.Number:
                    ConstantValue = Int32.Parse(token.Sequence);
                    break;
                case TokenEnum.Variable:
                    Coefficient = 1;
                    break;

                default:
                    throw new EvaluationException("Failed to parse token '{0}' to operand", token.Sequence);
            }

            DenominatorCoefficient = 0;
            DenominatorQuadraticCoefficient = 0;
            DenominatorConstantValue = 1;
        }

        /// <summary>
        /// Adds a <see cref="Calc.Domain.Operand"/> to a <see cref="Calc.Domain.Operand"/>, yielding a new <see cref="T:Calc.Domain.Operand"/>.
        /// by just adding the coefficients of each term.
        /// </summary>
        /// <param name="a">The first <see cref="Calc.Domain.Operand"/> to add.</param>
        /// <param name="b">The second <see cref="Calc.Domain.Operand"/> to add.</param>
        /// <returns>The <see cref="T:Calc.Domain.Operand"/> that is the sum of the values of <c>a</c> and <c>b</c>.</returns>
        public static Operand operator +(Operand a, Operand b)
        {
            a.Coefficient += b.Coefficient;
            a.ConstantValue += + b.ConstantValue;
            a.QuadraticCoefficient += b.QuadraticCoefficient;
            
            return a;
        }

		/// <summary>
		/// Subtracts a <see cref="Calc.Domain.Operand"/> from a <see cref="Calc.Domain.Operand"/>, yielding a new <see cref="T:Calc.Domain.Operand"/>.
		/// by simply deducing the coefficients of each term.
		/// </summary>
		/// <param name="a">Term a <see cref="Calc.Domain.Operand"/> to subtract from (the minuend).</param>
		/// <param name="b">Term b <see cref="Calc.Domain.Operand"/> to subtract (the subtrahend).</param>
		/// <returns>The <see cref="T:Calc.Domain.Operand"/> that is the <c>a</c> minus <c>b</c>.</returns>
		public static Operand operator -(Operand a, Operand b)
        {
            a.Coefficient -= b.Coefficient;
            a.ConstantValue -= b.ConstantValue;
            a.QuadraticCoefficient -= b.QuadraticCoefficient;
            return a;
        }

        /// <summary>
        /// Computes the product of <c>a</c> and <c>b</c>, yielding a new <see cref="T:Calc.Domain.Operand"/>.
        /// Currently, it doesn't allow the product to have a power higher than 2
        /// as it is not required
        /// </summary>
        /// <param name="a">Factor a<see cref="Calc.Domain.Operand"/> to multiply.</param>
        /// <param name="b">Factor b<see cref="Calc.Domain.Operand"/> to multiply.</param>
        /// <returns>The <see cref="T:Calc.Domain.Operand"/> that is the <c>a</c> * <c>b</c>.</returns>
        public static Operand operator *(Operand a, Operand b)
        {
            int c = a.QuadraticCoefficient,
                d = a.Coefficient,
                e = a.ConstantValue;

            int f = b.QuadraticCoefficient,
                g = b.Coefficient,
                h = b.ConstantValue;

            // If the product has a power higher than 2
            if ((c != 0 && (f != 0 || g != 0)) || (f != 0 && (c != 0 || d != 0)))
            {
                throw new EvaluationException("Solving equation with a power higher than 2 is not supported(required) yet.");
            }

            // Formula: (cx^2 + dx + e)(fx^2 + gx + h) = 
            a.QuadraticCoefficient = c * h + d * g + e * f;
            a.Coefficient = d * h + e * g;
            a.ConstantValue = e * h;

            return a;
        }

        /// <summary>
        /// Computes the division of <c>a</c> and <c>b</c>, yielding a new <see cref="T:Calc.Domain.Operand"/>.
        /// Currently, it dosen't allow b to have a variable,
        /// that is, solving fractional equation is not supported.
        /// </summary>
        /// <param name="a">The <see cref="Calc.Domain.Operand"/> to divide (the divident).</param>
        /// <param name="b">The <see cref="Calc.Domain.Operand"/> to divide (the divisor).</param>
        /// <returns>The <see cref="T:Calc.Domain.Operand"/> that is the <c>a</c> / <c>b</c>.</returns>
        public static Operand operator /(Operand a, Operand b)
        {
            if (b.Coefficient != 0 || b.QuadraticCoefficient != 0)
            {
                // if b is not only a constant, the equation will be a fractional equation.
                throw new EvaluationException("Solving fractional equation is not supported yet.");
            }
            else
            {
                if (b.ConstantValue != 0)
                {
                    a.ConstantValue /= b.ConstantValue;

                    if (a.Coefficient != 0)
                        a.DenominatorConstantValue = b.ConstantValue;
                }
                else
                {
                    throw new EvaluationException("Trying to divide by 0.");
                }
            }
            return a;
        }

        /// <summary>
        /// Overides the ^ operator to the exponential operator
        /// Does not support having a variable in the index
        /// </summary>
        /// <param name="a">The alpha component.</param>
        /// <param name="b">The blue component.</param>
        public static Operand operator ^(Operand a, Operand b)
        {
            if (b.Coefficient != 0 || b.QuadraticCoefficient != 0)
            {
                throw new EvaluationException("Solving exponential function is not supported(required) yet.");
            }

            // For case like: (2x + 3)^2
            // Formula (ax + b)^2 = (a^2)x^2 + 2abx + b ^ 2, where a can be 0
            if (a.QuadraticCoefficient == 0 && b.ConstantValue == 2)
            {
                a.QuadraticCoefficient = (int)Math.Pow(a.Coefficient, b.ConstantValue);
                a.Coefficient = 2 * a.Coefficient * a.ConstantValue;
                a.ConstantValue = (int)Math.Pow(a.ConstantValue, b.ConstantValue);
            }
            else if (a.Coefficient == 0 && a.QuadraticCoefficient == 0)
            {
                a.ConstantValue = (int)Math.Pow(a.ConstantValue, b.ConstantValue);
            }
            else
            {
                throw new EvaluationException("Solving equation with a power higher than 2 is not supported(required) yet.");
            }

            return a;
        }

        /// <summary>
        /// Modulo operation between operands
        /// Does not support having a variable in b
        /// </summary>
        /// <param name="a">The alpha component.</param>
        /// <param name="b">The blue component.</param>
        public static Operand operator %(Operand a, Operand b)
        { 
            if (a.QuadraticCoefficient != 0
                || b.QuadraticCoefficient != 0
                || a.Coefficient != 0
                || b.Coefficient != 0)
            {
                throw new EvaluationException("Modulo applied on variable is not supported.");
            }
            else
            {
				a.ConstantValue %= b.ConstantValue;
            }

            return a;
        }
    }
}
