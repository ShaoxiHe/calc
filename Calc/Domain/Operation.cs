﻿using System;
using Calc.Exceptions;

namespace Calc.Domain
{
    public class Operation
    {
        public Operation()
        {
        }

        /// <summary>
        /// Evaluate using the specified operation token, a and b.
        /// </summary>
        /// <returns>The evaluate.</returns>
        /// <param name="token">Token.</param>
        /// <param name="a">The alpha component.</param>
        /// <param name="b">The blue component.</param>
        public static Operand Evaluate(Token token, Operand a, Operand b)
        {
            Operand result = new Operand();

            switch (token.Sequence)
            {
                case "+":
                    result = a + b;
                    break;
                case "-":
                    result = a - b;
                    break;
                case "*":
                    result = a * b;
                    break;
                case "/":
                    result = a / b;
                    break;
                case "^":
                    result = a ^ b;
                    break;
                case "%":
                    result = a % b;
                    break;
                default:
                    throw new EvaluationException("Unexcepted operation symbol: '{0}'", token.Sequence);
            }

            return result;
        }
    }
}
