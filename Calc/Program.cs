﻿using System;

namespace Calc
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Calculator calculator = new Calculator();
            calculator.Accept(args);

            Console.WriteLine("Press any key to continue...");
            Console.Read();
        }
    }
}
