﻿using System;
namespace Calc.Enums
{
	public enum TokenEnum
	{
		Plusminus,
		Multdiv,
		Raised,
		OpenBracket,
		CloseBracket,
		Number,
		Variable,
		EqualSign,
        Modulo
	}
}
